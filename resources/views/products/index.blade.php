@extends('layouts')

@section('content')
<div class="container mt-5">
    <h1 class="mb-4">Products</h1>
    <a style="float: right;" href="{{ route('products.create') }}" class="btn btn-primary mb-3">Create Product</a>

    <table class="table table-bordered">
        <thead>
          <tr>
            <th>#</th>
            <th>Image</th>
            <th>Name</th>
            <th>Variants</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
           @foreach($products as $product)
            <tr>
                <td>{{$loop->iteration}}</td>
                <td>  
                    <img src="{{ asset('storage/' . $product->main_image) }}" alt="{{ $product->title }}" class="img-thumbnail mb-3" width="150">
                </td>
                <td>{{ $product->title }}</td>
                <td>
                    <ul class="list-group mt-3">
                        @foreach($product->variants as $variant)
                            <li class="list-group-item">Size:-{{ $variant->size }} | Color:-{{ $variant->color }}</li>
                        @endforeach
                    </ul>
                </td>
                <td>
                    <a href="{{ route('products.edit', $product) }}" class="btn btn-secondary btn-sm">Edit</a>
                    <form action="{{ route('products.destroy', $product) }}" method="POST" class="d-inline">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                    </form>
                </td>
            </tr>
         @endforeach
        </tbody>
      </table>

</div>
@endsection