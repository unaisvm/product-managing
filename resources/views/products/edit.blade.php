@extends('layouts')

@section('content')
<div class="container mt-5">
    <h1 class="mb-4">Edit Product</h1>
    <form action="{{ route('products.update', $product) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="row">
        <div class="mb-3 col-6">
            <label for="title" class="form-label">Title</label>
            <input type="text" class="form-control" id="title" name="title" value="{{ $product->title }}" required>
        </div>
        <div class="mb-3 col-6">
            <label for="description" class="form-label">Description</label>
            <textarea class="form-control" id="description" name="description" rows="3" required>{{ $product->description }}</textarea>
        </div>
        <div class="mb-3 col-6">
            <label for="main_image" class="form-label">Main Image</label>
            <input type="file" class="form-control" id="main_image" name="main_image">
            <input type="hidden" id="filepath" name="image" value="{{$product->main_image}}">
            <img src="{{ asset('storage/' . $product->main_image) }}" alt="{{ $product->title }}" id="uploadedImage" class="img-thumbnail mt-2" width="150">
        </div>
        </div>
        <h3>Variants</h3>
        <div id="variants" class="mb-3">
            @foreach($product->variants as $variant)
                <div class="variant mb-3">
                    <div class="row">
                        <div class="col">
                            <label for="size" class="form-label">Size</label>
                            <input type="text" class="form-control" name="variants[{{ $loop->index }}][size]" value="{{ $variant->size }}" required>
                        </div>
                        <div class="col">
                            <label for="color" class="form-label">Color</label>
                            <input type="text" class="form-control" name="variants[{{ $loop->index }}][color]" value="{{ $variant->color }}" required>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div style="float: right;">
        <button type="button" id="add-variant" class="btn btn-secondary mb-3">Add Variant</button>
        </div><br>
        <button type="submit" class="btn btn-primary">Update</button>
    </form>
</div>
@endsection

@section('scripts')

<script>
    $(document).ready(function() {
        $('#main_image').on('change', function() {
            var formData = new FormData();
            formData.append('image', $(this)[0].files[0]);

            $.ajax({
                url: '{{ route("image.upload") }}',
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(response) {
                    if(response.success) {
                        $('#uploadedImage').attr('src', response.file);
                        $('#filepath').val(response.path);
                        // $('#uploadedImageContainer').show();
                    }else{
                      
                        // $('#uploadedImageContainer').hide();
                        console.log(response);

                    }
                },
                error: function(response) {
                    alert('er');

                }
            });
        });
    });
</script>
    <script>
        document.getElementById('add-variant').addEventListener('click', function() {
            var variants = document.getElementById('variants');
            var index = variants.getElementsByClassName('variant').length;
            var variant = document.createElement('div');
            variant.classList.add('variant', 'mb-3');
            variant.innerHTML = `
                <div class="row">
                    <div class="col">
                        <label for="size" class="form-label">Size</label>
                        <input type="text" class="form-control" name="variants[${index}][size]" required>
                    </div>
                    <div class="col">
                        <label for="color" class="form-label">Color</label>
                        <input type="text" class="form-control" name="variants[${index}][color]" required>
                    </div>
                </div>
            `;
            variants.appendChild(variant);
        });
    </script>
@endsection