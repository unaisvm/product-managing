<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Variant;
use DB;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::with('variants')->get();
        return view('products.index', compact('products'));
    }

    public function create()
    {
        return view('products.create');
    }

    public function store(Request $request)
    {
     //  return $request;
   
        $product = new Product();
        $product->title=$request->title;
        $product->description=$request->description;
        $product->main_image=$request->image;
        $product->save();

        foreach ($request->variants as $variant) {
            $prod_var=new Variant();
            $prod_var->product_id=$product->id;
            $prod_var->size=$variant['size'];
            $prod_var->color=$variant['color'];
            $prod_var->save();
        }
       
      return redirect()->route('products.index');
  

       
    }

    public function edit(Product $product)
    {
        return view('products.edit', compact('product'));
    }

    public function update(Request $request, Product $product)
    {
        $product->title=$request->title;
        $product->description=$request->description;
        $product->main_image=$request->image;
        $product->update();

        $product->variants()->delete();
        foreach ($request->variants as $variant) {
            $prod_var=new Variant();
            $prod_var->product_id=$product->id;
            $prod_var->size=$variant['size'];
            $prod_var->color=$variant['color'];
            $prod_var->save();
        }
        return redirect()->route('products.index');
    }

    public function destroy(Product $product)
    {
        $product->variants()->delete();
        Storage::delete($product->main_image);
        $product->delete();
        return redirect()->route('products.index');
    }

    public function imageUpload(Request $request)
    {
        if ($request->hasFile('image')) {
            $path = $request->file('image')->store('images', 'public');
            $file=asset('storage/'.$path);
            return response()->json(['success'=>1,'path' =>$path,'file'=>$file]);
        }

        return response()->json(['error' => 'No file uploaded'], 400);
    }
}
